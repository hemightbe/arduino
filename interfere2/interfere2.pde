const int colorCount = 3;    // the number of colors

int ledPins[] = { 
  9, 10, 11};   // an array of pin numbers to which LEDs are attached

int delVal = 4;
int fadeVal = 255;
int incVal = 0;
int incAmount = 1;
boolean switch50 = LOW;
boolean switch100 = LOW;

void setup() {
  // loop over the pin array and set them all to output:
  for (int thisLed = 0; thisLed < colorCount; thisLed++) {
    pinMode(ledPins[thisLed], OUTPUT); 
    }
  }

void loop()  { 
     for (int i = 0; i < colorCount; i++) {                    // Count through all 6 colors and load those row values
       for(incVal= 5; incVal <= 255; incVal+=incAmount) {      // Start at 0 and increment by incAmount until value is 255 
         for (int j = 0; j < 2; j++) {                         // cycle between both columns 
           analogWrite(ledPins[0], incVal);
         }
       delay(delVal);                                          // wait for delVal 
      } 
       for(fadeVal= 255; fadeVal >= 5; fadeVal-=incAmount) {   // Start at 255 and decrese by incAmount until value is 0
         for (int j = 0; j < 2; j++) {                         // cycle between both columns 
           analogWrite(ledPins[0], fadeVal); 
         }
        delay(delVal);                                         // wait for delVal
     }
     
      if (switch100 = LOW) {
             delay(50);
             switch50 = HIGH;
           }
           else {
              switch50 = LOW;
         }; 
       for (int i = 0; i < colorCount; i++) {                    // Count through all 6 colors and load those row values
       for(incVal= 5; incVal <= 255; incVal+=incAmount) {      // Start at 0 and increment by incAmount until value is 255 
         for (int j = 0; j < 2; j++) {                         // cycle between both columns 
           analogWrite(ledPins[1], incVal);
         }
       delay(delVal);                                          // wait for delVal 
      } 
       for(fadeVal= 255; fadeVal >= 5; fadeVal-=incAmount) {   // Start at 255 and decrese by incAmount until value is 0
         for (int j = 0; j < 2; j++) {                         // cycle between both columns 
           analogWrite(ledPins[1], fadeVal); 
         }
        delay(delVal);                                         // wait for delVal
     }
          
      if (switch100 = LOW) {
             delay(50);
             switch100 = HIGH;
           }
           else {
              switch100 = LOW;
         }; 
     for (int i = 0; i < colorCount; i++) {                    // Count through all 6 colors and load those row values
       for(incVal= 5; incVal <= 255; incVal+=incAmount) {      // Start at 0 and increment by incAmount until value is 255 
         for (int j = 0; j < 2; j++) {                         // cycle between both columns 
           analogWrite(ledPins[2], incVal);
         }
       delay(delVal);                                          // wait for delVal 
      } 
       for(fadeVal= 255; fadeVal >= 5; fadeVal-=incAmount) {   // Start at 255 and decrese by incAmount until value is 0
         for (int j = 0; j < 2; j++) {                         // cycle between both columns 
           analogWrite(ledPins[2], fadeVal); 
         }
        delay(delVal);                                         // wait for delVal
     }

}
}}}

