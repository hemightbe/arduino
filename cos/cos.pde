const int ledRed = 9;
const int ledGreen = 10;
const int ledBlue = 11;

double redLevel;
double greenLevel;
double blueLevel;


void setup()
{
 pinMode(ledRed, OUTPUT);
 pinMode(ledGreen, OUTPUT);
 pinMode(ledBlue, OUTPUT);
}

void loop()
{
    int value = analogRead(analogPin);
    double RGBslider = (double)value/1024.0;
    
    redLevel   = 128.0 * ( 1 + cos( 2 * PI * (RGBslider + 0.125)));
    greenLevel = 128.0 * ( 1 + cos( 2 * PI * (RGBslider + 0.375)));
    blueLevel  = 128.0 * ( 1 + cos( PI * RGBslider));
    
    if (redLevel > 255) redLevel = 255;
    if (redLevel < 0) redLevel = 0;
    if (greenLevel > 255) greenLevel = 255;
    if (greenLevel < 0) greenLevel = 0;
    if (blueLevel > 255) blueLevel = 255;
    if (blueLevel < 0) blueLevel = 0;
    
    analogWrite(ledRed, 255 - ((byte)redLevel)); // negating the output for common anode (sinking)
    analogWrite(ledGreen, 255 - ((byte)greenLevel));
    analogWrite(ledBlue, 255 - ((byte)blueLevel));
}  

