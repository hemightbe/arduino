const int ledCount = 3;    // the number of LEDs in the bar graph
int ledPins[] = { 
  8, 9, 10};   // an array of pin numbers to which LEDs are attached
int sensorPin = A0;                  // select the input pin for the potentiometer
float del = 0.0000;
float delOn = 0.0000;   // initialize value for delayON
float delOff = 0.0000;   // initialize value for delayOff
int thisLed [] = {
  0, 1, 2};
int num = 0;
int RGB[7][3] = {          // an array of output pins for integers from 0 to 9 when the LEDs are connected to digital pins 2-9
  {0},       
  {1},       
  {2},      
  {0,1},    
  {0,2},  
  {1,2}, 
  {0,1,2}
}; 


void setup() {
  // loop over the pin array and set them all to output:
  for (int thisLed = 0; thisLed < ledCount; thisLed++) {
    pinMode(ledPins[thisLed], OUTPUT); 
  }
   del = (analogRead(sensorPin));
   delOn = (( 100.0000 / 1023.0000) * del);
   delOff= (((-100.0000 / 1023.0000) * del) + 100.0000);
}

void loop() {                                
   del = (analogRead(sensorPin));
   delOn = (( 100.0000 / 1023.0000) * del);
   delOff= (((-100.0000 / 1023.0000) * del) + 100.0000); 
   
   for(int i = 0; i < 7; i++) {
     for(int j = 0; j < ledCount; j++) {
       digitalWrite(ledPins[RGB[i][j]], HIGH);
       delay(delOn);
       digitalWrite(ledPins[RGB[i][j]], LOW);
       delay(delOn);
   }    
   }    
}
