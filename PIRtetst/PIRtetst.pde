//the time we give the sensor to calibrate (10-60 secs according to the datasheet)
int calibrationTime = 3000;        
int delVal = 5;
int delOff = 20;
int pirPin = 3;    //the digital pin connected to the PIR sensor's output
int ledPin = 10;
int i  =  0;

/////////////////////////////
//SETUP
void setup(){
  pinMode(pirPin, INPUT);
  pinMode(ledPin, OUTPUT);
  digitalWrite(pirPin, LOW);
  delay(calibrationTime);
  }

////////////////////////////
//LOOP
void loop(){

  if(digitalRead(pirPin) == HIGH){
   if (i < 255){
    for (i; i < 255; i++) { 
    analogWrite(ledPin, i);
    delay(delVal);
    }
   }   //the led visualizes the sensors output pin state
  }  
  

  if(digitalRead(pirPin) == LOW){       
   delay (8000);
    if (i > 0){
    for (i; i > 0; i--) { 
      analogWrite(ledPin, i);
      delay(delOff);
    }   //the led visualizes the sensors output pin state
   }
  }
 }
