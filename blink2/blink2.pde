//blink2


int ledPin0 = 9;            // LED connected to digital pin 9
int ledPin1 = 10;           // LED connected to digital pin 10
int ledPin2 = 11;           // LED connected to digital pin 11
int delVal0 = 1;            // LED0 down delay value
int delVal1 = 1;            // LED1 down delay value
int delVal2 = 1;            // LED2 down delay value
int upVal0 = 1;             // LED0 up delay value
int upVal1 = 1;             // LED1 up delay value
int upVal2 = 1;             // LED2 up delay value

void setup() {                
  // initialize the digital pins aas output
  pinMode(9, OUTPUT);   
  pinMode(10, OUTPUT);   
  pinMode(11, OUTPUT);
  upVal0 = random(50,300);    // Random number for LED0 up delay value
  upVal1 = random(50,300);    // Random number for LED1 up delay value
  upVal2 = random(50,300);    // Random number for LED2 up delay value
  delVal0 = random(50,300);    // Random number for LED0 down delay value
  delVal1 = random(50,300);    // Random number for LED1 down delay value
  delVal2 = random(50,300);    // Random number for LED2 down delay value
}

void loop() {
  upVal0 = random(50,300);    // Random number for LED0 up delay value
  digitalWrite(ledPin0, HIGH);   // set the LED on
  delay(upVal0);
  delVal0 = random(50,300);    // Random number for LED0 down delay value
  digitalWrite(ledPin0, LOW);    // set the LED off
  delay(delVal0);
  upVal1 = random(50,300);    // Random number for LED1 up delay value
  digitalWrite(ledPin1, HIGH);   // set the LED on
  delay(upVal1);
  delVal1 = random(50,300);    // Random number for LED1 down delay value
  digitalWrite(ledPin1, LOW);    // set the LED off
  delay(delVal1);
  upVal2 = random(50,300);    // Random number for LED2 up delay value
  digitalWrite(ledPin2, HIGH);   // set the LED on
  delay(upVal2);
  delVal2 = random(50,300);    // Random number for LED2 down delay value
  digitalWrite(ledPin2, LOW);    // set the LED off
  delay(delVal2);
}
