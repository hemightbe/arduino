const int ledCount = 3;    // the number of LEDs in the bar graph
int ledPins[] = { 
  8, 9, 10};   // an array of pin numbers to which LEDs are attached
int sensorPin = A0;                  // select the input pin for the potentiometer
float del = 0.0000;
float delOn = 0.0000;   // initialize value for delayON
float delOff = 0.0000;   // initialize value for delayOff
int thisLed [] = {
  0, 1, 2, 3};
int num = 0;
int RGB[7][3] = {          // an array of output pins for integers from 0 to 9 when the LEDs are connected to digital pins 2-9
  {1},       
  {2},       
  {3},      
  {1,2},    
  {1,3},  
  {2,3}, 
  {1,2,3}
}; 


void setup() {
  // loop over the pin array and set them all to output:
  for (int thisLed = 0; thisLed < ledCount; thisLed++) {
    pinMode(ledPins[thisLed], OUTPUT); 
  }
   del = (analogRead(sensorPin));
   delOn = (( 10.0000 / 1023.0000) * del);
   delOff= (((-10.0000 / 1023.0000) * del) + 10.0000);
}

void loop() {                                
   del = (analogRead(sensorPin));
   delOn = (( 10.0000 / 1023.0000) * del);
   delOff= (((-10.0000 / 1023.0000) * del) + 10.0000); 
   
   for(int i = 0; i < 8; i++) {
     for(int j = 0; j < ledCount; j++) {
          for(int i = 255; i >= 0; i--) { 
     for(int j = 0; j < ledCount; j++) {
     analogWrite(ledPins[RGB[i][j]], i);
     delay(delOff); 
       //digitalWrite(ledPins[RGB[i][j]], HIGH);
       delay(delOn);
      }
       for(int j = 0; j < ledCount; j++) {
       digitalWrite(ledPins[RGB[i][j]], LOW);
       delay(delOff);
      }  
   }  
}
