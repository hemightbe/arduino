/*
 * Code for cross-fading 3 LEDs, red, green and blue, or one tri-color LED, using PWM
 * The program cross-fades slowly from red to green, green to blue, and blue to red
 * originally "dimmingLEDs" by Clay Shirky <clay.shirky@nyu.edu>
 */

// Output
int redPin   = 9;   // Red LED,   connected to digital pin 9
int greenPin = 10;  // Green LED, connected to digital pin 10
int bluePin  = 11;  // Blue LED,  connected to digital pin 11

// Program variables
int redVal   = 1; // Variables to store the values to send to the pins
int greenVal = 1;   // Initial values are all off
int blueVal  = 1;

int i = 0;     // Loop counter    
int wait = 15; // 50ms (.05 second) delay; shorten for faster fades

void setup()
{
  pinMode(redPin,   OUTPUT);   // sets the pins as output
  pinMode(greenPin, OUTPUT);  
  pinMode(bluePin,  OUTPUT);
}

// Main program
void loop()
{
  i += 1;      // Increment counter
  {
  redVal =((i<255)*i)+((i>=255)*255)+((i>511)*(512-i))+((i>766)*(i-766))+((i>=1276)*(i-1276))+((i>1530)*(1530-i))+((i>1786)*(1786-i));
  greenVal =(i<256)*(1)+(i>255)*(i-255)+(i>510)*(510-i)+(i>1020)*(1020-i)+(i>1274)*(i-1274)+(i>1530)*(i-1531)+(i>1785)*(3571-(2*i));
  blueVal =(i<764)*(1)+(i>765)*(i-765)+(i>1020)*(1020-i)+(i>1786)*(1786-i);
  }

  if (i > 2040) //
  {
    i = 1;
  }  

  // we do "255-redVal" instead of just "redVal" because the
  // LEDs are hooked up to +5V instead of Gnd
  analogWrite(redPin,   redVal);   // Write current values to LED pins
  analogWrite(greenPin, greenVal);
  analogWrite(bluePin,  blueVal);  

  delay(wait); // Pause for 'wait' milliseconds before resuming the loop
} 

