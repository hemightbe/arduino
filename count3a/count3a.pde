// 8-segment LED display integer count test

int num[10][8] = {          // an array of output pins for integers from 0 to 9 when the LEDs are connected to digital pins 2-9
  {1, 2, 3, 5, 6, 7},       // 0  
  {3, 5},                   // 1
  {1, 2, 5, 6, 8},          // 2
  {2, 3, 5, 6, 8},          // 3
  {3, 5, 7, 8},             // 4
  {2, 3, 6, 7, 8},          // 5
  {1, 2, 3, 6, 7, 8},       // 6
  {3, 5, 6},                // 7
  {1, 2, 3, 5, 6, 7, 8},    // 8
  {3, 5, 6, 7, 8}           // 9
};

int ledPins[] = { 
  0, 2, 3, 4, 5, 6, 7, 8, 9};   	// an array of pin numbers to which the LEDs of the display are attached and a null pin, 0

int s1 = 10; 

boolean s1State = false;

void setup() {
  for (int i = 0; i < 8; i++) {   	// loop over the pin array and set them all to output
    pinMode(ledPins[i], OUTPUT);
  }
   pinMode(s1, INPUT);
}
  
void loop() {					
  
   for (int n = 0; n < 10; n++) {		// loop to turn on the required LEDs to display n by moving to num[n][] for every integer N, where N = n and 0 >= N <= 9
     if (digitalRead(s1) == HIGH) { 
        s1State = !s1State;                // toggle s1State variable
      } 
    for (int j = 0; j < 8; j++) {		// loop to decide outputs of the LEDs attached to the digital pinbs as defined by num[n][j]
      if (digitalRead(s1) == HIGH) { 
        s1State = !s1State;                // toggle s1State variable
      } 
      digitalWrite(ledPins[num[n][j]], HIGH);   // set pin defined by num[n][j] to HIGH
      if (s1State == true) {
         delay(100);                        // delay to debounce switch
      }
    }						// this occurs virtually instantaneously during execution so the clock remains relatively accurate
    
    delay(1000);                  	        // wait 1000 milliseconds
    
    for (int j = 0; j < 9; j++) {           	// set all outputs to the LEDs to LOW
      if (digitalRead(s1) == HIGH) { 
        s1State = !s1State;                // toggle s1State variable
      }
      digitalWrite(ledPins[j], LOW);		
         if (s1State == true) {
            delay(100);                        // delay
      }
    }
  }
}
