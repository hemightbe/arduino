// fades Spectrum cycle

int rpin = 11;      //i/o pin declarations
int gpin = 9;
int bpin = 10;
int potPin = 0;
float delVal = 1000;
float calibrate = 50.000000;

void setup()		    
{
//empty
}

void loop() {
  int r, g, b;
 
  // fade from blue to violet
  for (r = 0; r < 256; r++) { 
    analogWrite(rpin, r);
    delVal = (analogRead(potPin) / calibrate);
    delay(delVal);
  } 
  // fade from violet to red
  for (b = 255; b > 0; b--) { 
    analogWrite(bpin, b);
    delVal = (analogRead(potPin) / calibrate);
    delay(delVal);
  } 
  // fade from red to yellow
  for (g = 0; g < 256; g++) { 
    analogWrite(gpin, g);
    delVal = (analogRead(potPin) / calibrate);
    delay(delVal);
  } 
  // fade from yellow to green
  for (r = 255; r > 0; r--) { 
    analogWrite(rpin, r);
    delVal = (analogRead(potPin) / calibrate);
    delay(delVal);
  } 
  // fade from green to teal
  for (b = 0; b < 256; b++) { 
    analogWrite(bpin, b);
    delVal = (analogRead(potPin) / calibrate);
    delay(delVal);
  } 
  // fade from teal to blue
  for (g = 255; g > 0; g--) { 
    analogWrite(gpin, g);
    delVal = (analogRead(potPin) / calibrate);
    delay(delVal);
  } 
}
