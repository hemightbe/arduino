int ledPins[] = { 
  9, 10, 11};   // an array of pin numbers to which LEDs are attached
int sensorPin = A0;                  // select the input pin for the potentiometer
int del = 0;   // initialize value for delay
int fadeVal[] = { 
  0, 1, 2};   // an array of fade amounts
  int incVal[] = { 
  0, 1, 2};   // an array of increment amounts
  
void setup() {
  // loop over the pin array and set them all to output:
  for (int thisLed = 0; thisLed < 3; thisLed++) {
    pinMode(ledPins[thisLed], OUTPUT); 
  for (int thisFade = 0; thisFade < 5; thisFade++) {
  fadeVal[thisFade] = 0;
 del = (((analogRead(sensorPin) + 32) / 32)); 
}  
}

  for (int thisInc = 0; thisInc < 3; thisInc++) {  // Random number for the increment values
  incVal[thisInc] = 1; 
  }
}

void loop() {                                          // Loop for foward and reverd LEd lighting
  for (int thisLed = 0; thisLed <= 2; thisLed++) {     // start first lighting sequence for LEDs 0 through 5
    for(fadeVal[thisLed] = 0; fadeVal[thisLed] <= 255; fadeVal[thisLed] +=incVal[thisLed]) { 
      analogWrite(ledPins[thisLed], fadeVal[thisLed]);  
      del = (((analogRead(sensorPin) + 32) / 32));
      delay(del);   
    }
  delay(del);                                          // wait based on adjusted A0 input
     for(fadeVal[thisLed] = 255; fadeVal[thisLed] >= 0; fadeVal[thisLed] -=incVal[thisLed]) { 
     analogWrite(ledPins[thisLed], fadeVal[thisLed]);  
     del = (((analogRead(sensorPin) + 32) / 32));
     delay(del);   
    } 
  }
}

