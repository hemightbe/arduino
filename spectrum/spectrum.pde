const int colorCount = 6;    // the number of colors
int potPin = 1;
int ledPins[] = { 
  9, 10, 11};   // an array of pin numbers to which LEDs are attached 
                // Red Pin 9[0], Green pin 10[1], Blue Pin 11[2]
                // LED[Red,GND,Blue,Green]

int currentColor[6][2] = {
    {0,0},
    {0,1},
    {1,1},
    {1,2},
    {2,2},
    {2,0} };

int delVal = 27;
int fadeVal = 255;
int incVal = 0;
int incAmount = 1;
int decAmount = 2;

void setup() {
  // loop over the pin array and set them all to output:
  for (int thisLed = 0; thisLed < colorCount; thisLed++) {
    pinMode(ledPins[thisLed], OUTPUT); 
    }
  }

void loop()  { 
     for (int i = 0; i < colorCount; i++) {                    // Count through all 6 colors and load those row values
       for(incVal= 20; incVal <= 255; incVal+=incAmount) {      // Start at 0 and increment by incAmount until value is 255 
         for (int j = 0; j < 2; j++) {                         // cycle between both columns 
           analogWrite(ledPins[currentColor[i][j]], incVal);   // Write this to the chosen row column 
         };  
      delVal = (analogRead(potPin)+2);    // read the value from the sensor
       delay(delVal);                                          // wait for delVal 
      } 
       for(fadeVal= 255; fadeVal >= 20; fadeVal-=decAmount) {   // Start at 255 and decrese by incAmount until value is 0
         for (int j = 0; j < 2; j++) {                         // cycle between both columns 
           analogWrite(ledPins[currentColor[i][j]], fadeVal);  // Write this to the chosen row column  
         }
       delVal = (analogRead(potPin)+2);    // read the value from the sensor
       delay(delVal);                  // wait for delVal
     }
  }
}

