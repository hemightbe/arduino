int ledPin0 = 9;               // LED connected to digital pin 9
int ledPin1 = 10;              // LED connected to digital pin 10
int ledPin2 = 11;              // LED connected to digital pin 10
int delVal0 = 1;    // LED0 delay value
int delVal1 = 1;    // LED1 delay value
int delVal2 = 1;    // LED2 delay value
int incVal0 = 1;    // LED0 increment value
int incVal1 = 1;    // LED1 increment value
int incVal2 = 1;    // LED2 increment value
int decVal0 = 1;    // LED0 decrement value
int decVal1 = 1;    // LED1 decrement value
int decVal2 = 1;    // LED2 decrement value

void setup()  { 
  delVal0 = random(5,20);    // Random number for LED0 delay value
  delVal1 = random(5,20);    // Random number for LED1 delay value
  delVal2 = random(5,20);    // Random number for LED2 delay value
  incVal0 = random(1,10);    // Random number for LED0 increment value
  incVal1 = random(1,10);    // Random number for LED1 increment value
  incVal2 = random(1,10);    // Random number for LED2 increment value
  decVal0 = random(1,10);    // Random number for LED0 decrement value
  decVal1 = random(1,10);    // Random number for LED1 decrement value
  decVal2 = random(1,10);    // Random number for LED2 decrement value 
} 

void loop()  { 
  // fade in from min to max in increments of 2 points:
    delVal0 = random(5,20);    // Random number for LED0 delay value
    incVal0 = random(1,10);    // Random number for LED0 increment value
    decVal0 = random(1,10);    // Random number for LED0 decrement value
  for(int fadeValue0 = 0 ; fadeValue0 <= 255; fadeValue0 +=incVal0) { 
    // sets the value (range from 0 to 255):
    analogWrite(ledPin0, fadeValue0);  
    delay(delVal0);     
  }
    delVal1 = random(5,20);    // Random number for LED1 delay value
    incVal1 = random(1,10);    // Random number for LED1 increment value
    decVal1 = random(1,10);    // Random number for LED1 decrement value
   for(int fadeValue1 = 0 ; fadeValue1 <= 255; fadeValue1 +=incVal1) { 
    // sets the value (range from 0 to 255):
    analogWrite(ledPin1, fadeValue1);   
    delay(delVal1);
  }
    delVal2 = random(5,20);    // Random number for LED2 delay value
    incVal2 = random(1,10);    // Random number for LED2 increment value
    decVal2 = random(1,10);    // Random number for LED2 decrement value
    for(int fadeValue2 = 0 ; fadeValue2 <= 255; fadeValue2 +=incVal2) { 
    // sets the value (range from 0 to 255):  
    analogWrite(ledPin2, fadeValue2); 
    delay(delVal2);     
  } 
    delVal0 = random(5,20);    // Random number for LED0 delay value
    incVal0 = random(1,10);    // Random number for LED0 increment value
    decVal0 = random(1,10);    // Random number for LED0 decrement value
    for(int fadeValue0 = 255 ; fadeValue0 >= 0; fadeValue0 -=decVal0) { 
    // sets the value (range from 0 to 255):
    analogWrite(ledPin0, fadeValue0);  
    delay(delVal0);                          
  }
    delVal1 = random(5,20);    // Random number for LED1 delay value
    incVal1 = random(1,10);    // Random number for LED1 increment value
    decVal1 = random(1,10);    // Random number for LED1 decrement value
    for(int fadeValue1 = 255 ; fadeValue1 >= 0; fadeValue1 -=decVal1) { 
    // sets the value (range from 0 to 255):
    analogWrite(ledPin1, fadeValue1);       
    delay(delVal1);                                
  }
    delVal2 = random(5,20);    // Random number for LED2 delay value
    incVal2 = random(1,10);    // Random number for LED2 increment value
    decVal2 = random(1,10);    // Random number for LED2 decrement value
    for(int fadeValue2 = 255 ; fadeValue2 >= 0; fadeValue2 -=decVal2) { 
    // sets the value (range from 0 to 255):    
    analogWrite(ledPin2, fadeValue2); 
    delay(delVal2);                             
  }
}

