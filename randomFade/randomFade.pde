const int ledCount = 10;    // the number of LEDs in the bar graph

int ledPins[] = { 
  2, 3, 4, 5, 6, 7, 8, 9, 10, 11};   // an array of pin numbers to which LEDs are attached

int delVal[] = { 
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9};   // an array of delay values
  
int incVal[] = { 
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9};   // an array of increment amounts
  
int decVal[] = { 
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9};   // an array of decrement amounts

int fadeVal[] = { 
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9};   // an array of fade amounts



void setup() {
  // loop over the pin array and set them all to output:
  for (int thisLed = 0; thisLed < ledCount; thisLed++) {
    pinMode(ledPins[thisLed], OUTPUT); 
  }
  for (int thisDel = 0; thisDel < ledCount; thisDel++) {  // Random number for the delay values
  delVal[thisDel] = random(1,20); 
  }
  for (int thisInc = 0; thisInc < ledCount; thisInc++) {  // Random number for the increment values
  incVal[thisInc] = random(1,10); 
  }
  for (int thisDec = 0; thisDec < ledCount; thisDec++) {  // Random number for the decrement values
  decVal[thisDec] = random(1,10); 
  }
  for (int thisFade = 0; thisFade < 12; thisFade++) {
  fadeVal[thisFade] = 0;
  }
}

void loop()  { 
    for (int thisPin = 0; thisPin < ledCount; thisPin++) {    // fade LEDs 0-9 on at random intervals and inrements
      for(fadeVal[thisPin] = 0; fadeVal[thisPin] <= 255; fadeVal[thisPin] +=incVal[thisPin]) { 
      analogWrite(ledPins[thisPin], fadeVal[thisPin]);  
      delay(delVal[thisPin]);  
      for(fadeVal[thisPin] = 255; fadeVal[thisPin] >= 0; fadeVal[thisPin] -=decVal[thisPin]) { 
      analogWrite(ledPins[thisPin], fadeVal[thisPin]);  
      } 
    }
  }
}
