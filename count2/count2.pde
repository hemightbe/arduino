int num[10][8] = {          // an array of output pins for integers from 0 to 9 when the LEDs are connected to digital pins 2-9
  {1, 2, 3, 5, 6, 7},       // 0  
  {3, 5},                   // 1
  {1, 2, 5, 6, 8},          // 2
  {2, 3, 5, 6, 8},          // 3
  {3, 5, 7, 8},             // 4
  {2, 3, 6, 7, 8},          // 5
  {1, 2, 3, 6, 7, 8},       // 6
  {3, 5, 6},                // 7
  {1, 2, 3, 5, 6, 7, 8},    // 8
  {3, 5, 6, 7, 8}           // 9
};

int ledPins[] = { 
  0, 2, 3, 4, 5, 6, 7, 8, 9};   	// an array of pin numbers to which the LEDs of the display are attached and a null pin, 0

void setup() {
  for (int i = 0; i < 8; i++) {   	// loop over the pin array and set them all to output
  pinMode(ledPins[i], OUTPUT);
  }
}
  
void loop() {					
  for (int n = 0; n < 10; n++) {		// loop to turn on the required LEDs to display n by moving to num[n][] for every integer N, where N = n and N >= 0 <= 9
    for (int j = 0; j < 8; j++) {		// loop to decide outputs the LEDs of the display
    digitalWrite(ledPins[num[n][j]], HIGH);    	// set pin defined by num[n][j] to HIGH
    delay(100);
    }						// this occurs virtually instantaneously during execution so the clock remains relatively accurate
    
    delay(1000);                  	        // wait 1000 milliseconds
   
    for (int j = 0; j < 9; j++) {           	// set all outputs to the LEDs to LOW
    digitalWrite(ledPins[j], LOW);		// set pin defined by j to LOW
    delay(100);
    }
  }
}
