#include <RGBConverter.h>
#include <Rainbowduino.h>

// color column storage
long columColors[8] = { 0, 0, 0, 0, 0, 0, 0, 0};
// x-position storage
unsigned int columnXpos[8] = { 0, 0, 0, 0, 0, 0, 0, 0};

// asyncronous column timing storage
long columnTimers[8] = {0, 0, 0, 0, 0, 0, 0, 0};
// column animations speed storage
long columnDelays[8] = {0, 0, 0, 0, 0, 0, 0, 0};

// animation speed range
int minDelay = 75;
int maxDelay = 150;

// color vars
byte rgb[3] = {0, 0, 0};
double hsv[3] = {0, 0, 0};
double saturation = 1; // use 0 for white pixels
RGBConverter conv;

void setup()
{
  //Serial.begin(9600);
  conv = RGBConverter();
    Rb.init();

  // set intial delay and color for each column
  for(int i = 0; i < 8; i++)  
  {
    columnDelays[i] = random(minDelay, maxDelay);
    columColors[i] = hsvToRgbValue(rand()/double(RAND_MAX), saturation, 1, rgb);
  }

  Rb.blankDisplay();
}

void loop()
{
  // loop through all matrix columns
  for(int i = 0; i < 8; i++)
  {
    // check for lapsed time
    if(millis() >= columnTimers[i]) 
    {
      // update the pixel
      columnXpos[i] = renderPixel(columnXpos[i], i, columColors[i]);

      // for the ghosting we need the hue of the original pixel
      double hue = rbgToHueValue(columColors[i], hsv);
      // render 3 ghosts, each with lower saturation
      renderGhost(columnXpos[i], i, hsvToRgbValue(hue, saturation, 0.25, rgb), 2);
      renderGhost(columnXpos[i], i, hsvToRgbValue(hue, saturation, 0.10, rgb), 3);
      renderGhost(columnXpos[i], i, hsvToRgbValue(hue, saturation, 0.01, rgb), 4);

      // pixel reached the top again, randomly reset speed and color
      if(columnXpos[i] == 0) 
      {
        columnDelays[i] = random(minDelay, maxDelay);
        columColors[i] = hsvToRgbValue(rand()/double(RAND_MAX), saturation, 1, rgb);
      }

      // update the column timer with delay
      columnTimers[i] += columnDelays[i];
    }
  }
}

// renders a single pixel, remove pixel from previous cycle
int renderPixel(int x, int y, long color)
{
  // remove the pixel from previous cycle
  int clean = x == 0 ? 7 : x-1;
  Rb.setPixelXY(clean, y, 0, 0, 0);

  // draw new pixel
  Rb.setPixelXY(x, y, color);
  x = (x == 7) ? 0 : x+1;
  return x;
}

// renders a following/ghosting pixel
void renderGhost(int x, int y, long color, int dist)
{
  //simply shift xpos by dist
  x = x < dist ? x-dist+8 : x-dist;

  // render pixel with new x position
  renderPixel(x, y, color);
}

// takes hue, saturation and value and returns a combined rgb color
long hsvToRgbValue(double hue, double sat, double val, byte rgb[3])
{
  // get and array of rgb colors
  conv.hsvToRgb(hue, sat, val, rgb);

  // merge array of rgb color to a single value
  long c = rgb[0]; // red
  c = c << 8 | rgb[1]; // green
  c = c << 8 | rgb[2]; // blue  

  return c;
}

// takes an rgb color and returns its hue
double rbgToHueValue(double color, double hsv[3])
{
  // convert rgb color to hsv values
  conv.rgbToHsv(long(color) >> 16 & 0xFF, long(color) >> 8 & 0xFF, long(color) & 0xFF, hsv);

  // return the hue part only
  return hsv[0];
}
