const int ledCount = 3;    // the number of LEDs in the bar graph
int ledPins[] = { 
  8, 9, 10, 11};   // an array of pin numbers to which LEDs are attached
int sensorPin = A0;                  // select the input pin for the potentiometer
int del = 0;   // initialize value for delay
int thisLed [] = {
  0, 1, 2, 3};
int num = 0;
 
int RGB[8][4] = {          // an array of output pins for integers from 0 to 9 when the LEDs are connected to digital pins 2-9
  {0,0,0,0},       // 0  
  {0,1,0,0},       // 1
  {0,0,2,0},      // 2
  {0,0,0,3},      // 3
  {0,1,2,0},    // 4
  {0,1,3,0},    // 5
  {0,0,2,3},   // 6
  {0,1,2,3}, // 7
};
 
void setup() {
  // loop over the pin array and set them all to output:
  for (int thisLed = 0; thisLed < 3; thisLed++) {
    pinMode(ledPins[thisLed], OUTPUT); 
    del = (((analogRead(sensorPin) + 32) / 32));  
  }
}

void loop() {                                
    num = random(1,8);
    
    for(int i = 0; i <= 255; i++) {
     for(int j = 0; j < ledCount; j++) {
     analogWrite(ledPins[RGB[num][j]], i);
    }  
      del = (analogRead(sensorPin) / 32);
      delay(del);   
    }                                
     
     for(int i = 255; i >= 0; i--) { 
     for(int j = 0; j < ledCount; j++) {
     analogWrite(ledPins[RGB[num][j]], i);
   }
     del = (analogRead(sensorPin) / 32);
     delay(del);   
    } 
}

