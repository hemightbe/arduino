int ledPins[] = { 
  2, 3, 4, 5, 6, 7};   // an array of pin numbers to which LEDs are attached
int sensorPin = A0;    // select the input pin for the potentiometer
int del = 0;          // initialize value for delay
  
void setup() {
  // loop over the pin array and set them all to output:
  for (int thisLed = 0; thisLed < 6; thisLed++) {
    pinMode(ledPins[thisLed], OUTPUT); 
  }
}

void loop() {                                          // Loop for foward and reverd LEd lighting
  del = ((analogRead(sensorPin) / 3) + 35);            // read value of sensor pin and adjust for delay value
  for (int thisLed = 0; thisLed <= 5; thisLed++) {     // start first lighting sequence for LEDs 0 through 5
  digitalWrite(ledPins[thisLed], HIGH);                 // turn LEDs on
  delay(del);                                          // wait based on adjusted A0 input
  // digitalWrite(ledPins[thisLed], LOW);              // turn LEDs off
  }
  for (int thisLed = 5; thisLed > -1; thisLed--) {    // start second lighting sequence for LEDs 4 through 1
 // digitalWrite(ledPins[thisLed], HIGH);            // turn LEDs on
  delay(del);                                       // wait based on adjusted A0 input
  digitalWrite(ledPins[thisLed], LOW);              // turn LEDs off
  }
  delay(del);                                      // delay for thisLed[0] LOW
}

