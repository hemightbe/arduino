int r = 9;      //i/o pin declarations
int g = 10;
int b = 11;

float rBright = 0;
float gBright = 0;
float bBright = 0;

void setup()		    
{
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
} 

void loop () {
 
 rBright = (analogRead(A1) / 4.0000);
 analogWrite(r, rBright); 
 delay(1);
 gBright = ((1023.0000 - analogRead(A1)) / 4.0000);
 analogWrite(g, gBright); 
 delay(1);
 bBright = (analogRead(A1) / 4.0000);
 analogWrite(r, bBright); 
 delay(1);
  
}
