// displayTest


const int ledCount = 8;    // the number of LEDs in the bar graph
int ledPins[] = { 
  2, 3, 4, 5, 6, 7, 8, 9};   // an array of pin numbers to which LEDs are attached
int delVal[] = { 
  0, 1, 2, 3, 4, 5, 6, 7};   // an array of delay values
int ranPin = 0;
int sensorPin = A0;    // select the input pin for the potentiometer
int offSet = 0;

void setup() {
                                                          // loop over the pin array and set them all to output:
  for (int thisLed = 0; thisLed < ledCount; thisLed++) {  
  pinMode(ledPins[thisLed], OUTPUT);
  }
  for (int thisDel = 0; thisDel < ledCount; thisDel++) {  // Random number for the delay values
  delVal[thisDel] = random(1,20); 
  }
  ranPin = random(0, 8);                  // choose a random output pin between 0 and 7
}

void loop()  {                                  // loop for random blinking
      // randomSeed(analogRead(1));                // use pin A1 for randomSeed
      offSet = (analogRead(sensorPin));         // set offSet from "random" number
      ranPin = random(0, 8);                  // choose a random output pin between 0 and 7
      delVal[ranPin] = random((10+offSet),(200+offSet));    // choose a random value for the and and off delay time for the LED between (10 + the random Offset)ms and (200 + Offsett)ms
      digitalWrite(ledPins[ranPin], HIGH);                 // turn on the selected LED
      delay(delVal[ranPin]);                 // wait the random delay time
      digitalWrite(ledPins[ranPin], LOW);    // turn the LED off
      delay(delVal[ranPin]);                // wait the random delay time
}


