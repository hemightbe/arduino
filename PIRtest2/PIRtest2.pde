//the time we give the sensor to calibrate (10-60 secs according to the datasheet)
int calibrationTime = 30000;        
int delVal = 5;
int pirPin = 3;    //the digital pin connected to the PIR sensor's output
int ledPin = 10;
int i  =  255;

/////////////////////////////
//SETUP
void setup(){
  pinMode(pirPin, INPUT);
  pinMode(ledPin, OUTPUT);
  digitalWrite(pirPin, LOW);
  delay(calibrationTime);
  }

////////////////////////////
//LOOP
void loop(){

do
{
  analogWrite(ledPin, i);
  i++;
  delay(delVal);
  } while (digitalRead(pirPin) == HIGH);
do
{
  analogWrite(ledPin, i);
  i++;
  delay(delVal);
  } while (digitalRead(pirPin) == LOW);
}


